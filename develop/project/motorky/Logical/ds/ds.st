PROGRAM _INIT
	DS_0.init.taskTime := 0.010;		(*task class in sec *)
	DS_0.init.units := 1000;
	DS_0.init.reverseDirection := 0;

	DS_0.param.position := 1000;
	DS_0.param.speed := 1000;
	DS_0.param.acceleration := 1000;
	DS_0.param.deceleration := 1000;
END_PROGRAM
	
PROGRAM _CYCLIC
DS_0();
 
CASE step OF
0:
	IF DS_0.info.initialized = 1 THEN
		step := step +1;
	END_IF

1:
	DS_0.cmd.power := 1;
	step := step +1;

2:
	IF DS_0.info.powerOn = 1 THEN
		step := step +1;
	END_IF

3:
	DS_0.cmd.homing := 1;
	step := step +1;

4:
	IF DS_0.info.homingOk = 1 THEN
		step := step +1;
	END_IF

5:
	IF go = 1 THEN
		step := step + 1;
	END_IF

6:
	DS_0.param.position := 3000;
	DS_0.cmd.absolute := 1;
	step := step + 1;

7:
	IF DS_0.info.moveActive = 0 THEN
		step := step +1;
	END_IF

8:
	DS_0.param.position := 0;
	DS_0.cmd.absolute := 1;
	step := step + 1;

9:
	IF DS_0.info.moveActive = 0 THEN
		step := 5;
	END_IF

END_CASE
END_PROGRAM

