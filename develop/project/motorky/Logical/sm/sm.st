PROGRAM _INIT
	SM_0.init.taskTime := 0.002;		(*task class in sec *)
	SM_0.init.units := 100; 			
	SM_0.init.reverseDirection :=0;
	SM_0.init.outputFilter := 0.1;

	SM_0.param.position := 100;  
	SM_0.param.speed := 250;
	SM_0.param.acceleration := 2000;
	SM_0.param.deceleration := 2000;
	
END_PROGRAM

PROGRAM _CYCLIC
	SM_0(); 

	CASE step OF
		0:
			IF SM_0.info.initialized = 1 THEN
				step := step + 1;
			END_IF

		1:
			SM_0.cmd.power := 1;
			step := step + 1;

		2:
			IF SM_0.info.powerOn = 1 THEN
				step := step + 1;
			END_IF

		3:
			SM_0.cmd.homing := 1;
			step := step + 1;

		4:
			IF SM_0.info.homingOk = 1 THEN
				step := step + 1;
			END_IF

		5:
			IF go = 1 THEN
				step := step + 1;
			END_IF

		6:
			SM_0.param.position := 100;
			SM_0.param.speed := 40;
			SM_0.cmd.absolute := 1;
			step := step + 1;

			TON_0.IN := TRUE;
			TON_0.PT := T#1s;

		7:
			IF SM_0.info.moveActive = 0 THEN
				step := step +1;
			END_IF

		8:
			SM_0.param.position := 0;
			SM_0.param.speed := 40;
			SM_0.cmd.absolute := 1;
			step := step + 1;

		9:
			IF SM_0.info.moveActive = 0 THEN
				step := 5;
			END_IF

	END_CASE
	
	TON_0();
	IF TON_0.Q THEN
		TON_0(IN:=FALSE);
		SM_0.param.speed := 60;
	END_IF
				
END_PROGRAM

