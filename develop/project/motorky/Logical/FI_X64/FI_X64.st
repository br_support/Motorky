(********************************************************************
 * COPYRIGHT --  
 ********************************************************************
 * Program: ACOPOSinverter
 * File: ACOPOSinverter.st
 * Author: vancurar
 * Created: March 22, 2011
 ********************************************************************
 * Implementation of program ACOPOSinverter
 ********************************************************************)

PROGRAM _INIT
	FI_0.init.taskTime := 0.002;
	FI_0.init.units := 1000;
	FI_0.init.encoderIncrements := 4000;
	FI_0.init.reverseDirection := 0;
	FI_0.init.maxLagError := 1000;
	FI_0.init.Kp := 1;
	FI_0.init.Tn := 0;
	FI_0.init.settleTime := 0;
	FI_0.init.positiveSwLimit := 0;
	FI_0.init.negativeSwLimit := 0;
	
	FI_0.param.position := 3000;
	FI_0.param.speed := 5000;
	FI_0.param.acceleration := 100000;
	FI_0.param.deceleration := 100000;
	
	EnableCounter01 := 1;

END_PROGRAM

PROGRAM _CYCLIC

	CASE StatusWord AND 16#00FF OF
	16#0040:
		ControlWord := 16#0006;

	16#0021:
		IF FI_0.IOMapping.DriveEnable THEN
			ControlWord := 16#0007;
		END_IF

	16#0023:
		ControlWord := 16#000F;

	16#0027:
		IF FI_0.IOMapping.DriveEnable = 0 THEN
			ControlWord := 16#0006;
		END_IF
		
	END_CASE


	CASE step OF
	0:
		IF FI_0.info.initialized = 1 THEN
			step := step +1;
		END_IF

	1:
		FI_0.cmd.power := 1;
		step := step +1;

	2:
		IF FI_0.info.powerOn = 1 THEN
			step := step +1;
		END_IF

	3:
		IF go = 1 THEN
			step := step + 1;
		END_IF

	4:
		FI_0.param.position := 3000;
		FI_0.cmd.absolute := 1;
		step := step + 1;

	5:
		IF FI_0.info.moveActive = 0 THEN
			step := step +1;
		END_IF

	6:
		FI_0.param.position := 0;
		FI_0.cmd.absolute := 1;
		step := step + 1;

	7:
		IF FI_0.info.moveActive = 0 THEN
			step := 3;
		END_IF

	END_CASE

	memcpy(ADR(FI_0.IOMapping.Encoder),ADR(Counter01),2);
	FI_0();

END_PROGRAM
