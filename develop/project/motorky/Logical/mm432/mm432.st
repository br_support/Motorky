PROGRAM _INIT
	FI_0.init.taskTime := 0.01;	(* task class in sec *)
	FI_0.init.units := 1;
	FI_0.init.increments := 1;
	FI_0.init.reverseDirection := 0;
	FI_0.init.maxSpeed := 7200;
	FI_0.init.maxAcceleration := 10000;
	FI_0.init.maxLagError := 1000;

	FI_0.param.settleTime := 0.5;
	FI_0.param.noEncoder := 0;
	FI_0.param.Kp := 0.2;
	FI_0.param.Tn := 0;
	
	ss2dw14out := 2#1000_1000_0000_0000;
	
	IOC2003_0.enable := 1;
	IOC2003_0.Slot := 1;
	IOC2003_0.SubSlot := 2;
	IOC2003_0.ConfigWord := 14;
	IOC2003_0.ReadWrite := 1;
	IOC2003_0.pData := ADR(ss2dw14out);
	
	REPEAT
		IOC2003_0();
	UNTIL 
		IOC2003_0.status = 0
	END_REPEAT
	
	enableOutput := 1;
	emergencyStop := 1;
	
END_PROGRAM



PROGRAM _CYCLIC
	
	memset(ADR(FI_0.IOMapping.ModuleOk),1,SIZEOF(FI_0.IOMapping.ModuleOk));
	memcpy(ADR(FI_0.IOMapping.Encoder),ADR(counter)+2,SIZEOF(FI_0.IOMapping.Encoder));
	
	FI_0();
	
	pwmOutput := FI_0.IOMapping.AnalogOutput;

END_PROGRAM
