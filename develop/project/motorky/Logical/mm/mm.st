PROGRAM _INIT
	MM_0.init.taskTime := 0.001;	(* task class in sec *)
	MM_0.init.units := 1000;
	MM_0.init.encoderIncrements := 1000;
	MM_0.init.reverseDirection := 0;
	MM_0.init.maxLagError := 2000;
	MM_0.init.settleTime := 0.1;
	MM_0.init.Kp := 50;
	MM_0.init.Tn := 0;
	MM_0.init.speedKp := 1;
	MM_0.init.speedTn := 0;
	MM_0.init.speedFilter := 0.002;

END_PROGRAM


PROGRAM _CYCLIC
	MM_0();
(*
	CASE step OF
	0:
		IF MM_0.info.initialized = 1 THEN
			step := step +1;
		END_IF

	1:
		MM_0.cmd.power := 1;
		step := step +1;

	2:
		IF MM_0.info.powerOn = 1 THEN
			step := step +1;
		END_IF

	3:
		MM_0.param.position := 0;
		MM_0.param.speed := 5000;
		MM_0.param.acceleration := 5000;
		MM_0.param.deceleration := 5000;
		MM_0.param.homing.mode := 1;
		MM_0.param.homing.edgeSwitch := 1;
		MM_0.cmd.homing := 1;
		step := step +1;

	4:
		IF MM_0.info.homingOk = 1 THEN
			step := step +1;
		END_IF

	5:
		MM_0.param.position := 10000;
		MM_0.param.speed := 4000;
		MM_0.param.acceleration := 50000;
		MM_0.param.deceleration := 50000;
		step := step +1;
		
	6:
		IF go = 1 THEN
			step := step + 1;
		END_IF

	7:
		MM_0.param.position := 3000;
		MM_0.cmd.absolute := 1;
		step := step + 1;

	8:
		IF MM_0.info.moveActive = 0 THEN
			step := step +1;
		END_IF

	9:
		MM_0.param.position := 0;
		MM_0.cmd.absolute := 1;
		step := step + 1;

	10:
		IF MM_0.info.moveActive = 0 THEN
			step := 5;
		END_IF

	END_CASE
	
	IF MM_0.info.status <> 0 THEN
		MM_0.cmd.acknowledge := 1;
		//once := 0;
	END_IF
	*)
END_PROGRAM

